Some commands to make a nice letter and cv in a unified design with scrlttr2
as documentclass.

There are some samples, almost everything is freely configurable. Just take a
look around and I am sure you will be able to customize what you want to
customize.

Localization works for english and german. If you want to provide some more
translation, please fork the project and make a pull request.

Note:
For the samples, there is a bib file. The doi-numbers were the first I could
find. They have no deeper meaning and only showcase how the dois will be
linked.

Happy job hunting!

Author: Marco Bungart (Turing85, marco.bungart@googlemail.com)

Acknowledgements:

Christoph Eickhoff and Lennert Raesch: For giving the inspiration of color schemes.

Jonas Posner: For the idea to integrate localization.